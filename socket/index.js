const socket = require('./libs/socket');
const config = require('../config');
const commands = require('../commands');
const Connectn = require('./libs/connection');
class WogSocketServer {
  constructor() {
    this.config = config;
    this.socket = new socket(this.config);
  }
  start() {
    this.socket.startSocket();
  }
  emitCommand(userid, message) {
    this.socket.emitCommand(userid, message)
  }
}

module.exports = WogSocketServer;
