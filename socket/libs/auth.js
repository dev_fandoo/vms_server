'use strict'
const DeviceAccessToken = require('../../auth/models/deviceTokenAccess');
const run = require('../../util/run');
class WogSocketAuth {
  authDevice(socket, next) {

    try {
      this._checkAuthToken(socket.handshake.query.auth_token, (userid) => {
        console.log(userid);
        if (userid) {
          console.log("Authenticated socket ", socket.id);
          socket.userid = userid;
          next();
        } else {
          console.log("Socket not Authenticated ", socket.id);
          next(new Error('Authentication error'));
        }
      });
      // });
    } catch (err) {
      console.error(err.stack);
      next(new Error('Internal server error'));
    }
  }

  _checkAuthToken(_token, cb) {
    run(function*() {
      let user = yield DeviceAccessToken.where('token', _token).find();
      if (user.length > 0) {
        cb(user[0].attributes._id)

      } else {
        cb(false);
      }
    });

  }

}
module.exports = WogSocketAuth;
