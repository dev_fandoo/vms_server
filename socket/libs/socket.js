const Auth = require('./auth');
const Conn = require('./connection')
const Io = require('socket.io');
const express = require('express');
class Socket {
  constructor(config) {
    this.config = config;
    this.app = express();
    this.SocketServer = require('http').Server(this.app);
    this.io = Io(this.SocketServer);
    this.auth = new Auth();
    this.conn = new Conn();
    this._init();
  }

  _init() {
    const rou = require('../../route/libs/routes');
    this.app.use(rou);
    this.app.use(express.static('public_html'));
  }

  startSocket() {
    this.SocketServer.listen(this.config.express.port, () => {
      console.log('Express listening on ', this.config.express.port);
    });
    this.io.use((socket, next) => this.auth.authDevice(socket, next));
    this.io.on('connection', (socket) => this.conn.handleConn(socket));
  }
  emitCommand(userid, message) {
    this.conn.emiter(userid, message, this.io);
  }
}
module.exports = Socket;
