const Commands = require('../commands');
const should = require('should');
describe('Test Static cmds', function() {
  it('should get static variables', function() {
    should(Commands.CHANGE_VEHICLE).be.exactly(36);
    should(Commands.UPDATE_ROUTE).be.exactly(65);
    should(Commands.UPDATE_SETTINGS).be.exactly(23);
  });
});
