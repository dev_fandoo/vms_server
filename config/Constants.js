const path = require('path');
class Constants {
    constructor() {
        this.wog_root_dir = process.cwd();
        this.imgUploadDirName = "uploads";
        this.img_ext = '.webp';
    }

    get imgUploadDir() {
        return path.join(this.wog_root_dir, this.imgUploadDirName);
    }
}
module.exports = Constants;
