class ExpressConfig {
    constructor() {
        this.production = process.env.NODE_ENV === 'production';
        this.port = process.env.EXPRESS_PORT || 8080;
        this.jwtSecret = '4asd44w66a44ss3';
    }
}


module.exports = ExpressConfig;