const ExpressConfig = require('./ExpressConfig');
const MongoConfig = require('./MongoConfig');
const Constants = require('./Constants');

class Config {
    constructor() {
        this.express = new ExpressConfig();
        this.mongo = new MongoConfig();
        this.constants = new Constants();
    }
}
module.exports = new Config();