class MongoConfig {
    constructor() {
        this.uri = 'mongodb://localhost:27017/vms_db'
    }
}

module.exports = MongoConfig;