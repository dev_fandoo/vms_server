module.exports = function(grunt) {
  grunt.initConfig({
    concurrent: {
      dev: {
        tasks: ['nodemon', 'watch'],
        options: {
          logConcurrentOutput: true
        }
      }
    },
    watch: {
      files: ['test/**/*.js'],
      tasks: ['simplemocha']
    },
    nodemon: {
      dev: {
        script: 'app.js',
        options: {
          ignore: ['node_modules/**/*.*', 'test/**/*.*']
        }
      },
    },
    simplemocha: {
      options: {
        ui: 'bdd',
        reporter: 'spec'
      },
      all: {
        src: ['test/**/*.js']
      }
    }
  });
  grunt.registerTask('default', ['concurrent']);
  grunt.loadNpmTasks('grunt-concurrent');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-nodemon');
  grunt.loadNpmTasks('grunt-simple-mocha');
};
