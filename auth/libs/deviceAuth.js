const TokenGenerator = require('../util/token');
class DeviceAuth {
    constructor(userid) {
        this.userid = userid
    }

    get setToken() {
        return TokenGenerator.gen(this.userid);
    }

}

module.exports = DeviceAuth;