const config = require('../../config');
const jwt = require('jwt-simple');
class Token {
    static gen(userid) {
        return jwt.encode({
            userid: userid,
            date: Date.now()
        }, config.express.jwtSecret);
    }
}
module.exports = Token;