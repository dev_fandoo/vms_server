const SetupDir = require('./libs/SetupDir');
const config = require('../config');
class Setup {
  constructor() {
    this.SetupDir = new SetupDir(config);
  }
}
module.exports = Setup;
