const fs = require('fs');

const path = require('path');
class SetupDir {
  constructor(config) {
    this.config = config;
    this._startSetup();
  }
  _startSetup() {
    fs.stat(this.config.constants.imgUploadDir, (err) => {
      if (err && err.code == 'ENOENT') fs.mkdir(this.constants.imgUploadDir);
      else if (err) {
        console.log(err);
      }
    });
  }
}
module.exports = SetupDir;
