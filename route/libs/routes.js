var express = require('express');
var device_route = express.Router();
const path = require('path');
const DeviceSetttings = require('../../devicesettings');
const Device = require('./../../device');
const Command = require('../../commands');
// user_route.get('/getdevice', UserController.getDevice);
// user_route.get('/login', UserController.login);
// user_route.get('/register', UserController.register);
device_route.post('/device/verify', function(req, res) {
  let query = req.body;
  let device = new Device();
  device.getDeviceById(query.device.id, (dev) => {
    if (dev) {
      device.verifyDevice(dev, (verified) => {
        console.log(verified);
        res.send("Device Verified");
      });
    }
  });

});
device_route.post('/device/register', function(req, res) {
  let query = req.body;
  device.saveDeviceInfo(query.device, (dev) => {
    if (dev) {
      console.log(dev);
      res.send("Device Saved");
    }
  });
});
device_route.get('/device/getsettings', function(req, res) {
  let query = req.query;
  let deviceSetting = new DeviceSetttings();
  deviceSetting.getDeviceSettings()
});
device_route.post('/device/updatesetting', function(req, res) {

  let command = new Command();
  command.createCommand({
    userid: 'id.userid',
    type: 'id.type',
    payload: 'id.payload'
  });
  command.socketEmit();
  res.send('DONE');
});

module.exports = device_route;
