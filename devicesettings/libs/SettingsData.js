const SettingsModel = require('../models/SettingModel');
const run = require('../../util/run');
const co = require('co');
class SettingData {
  constructor(config) {
    this.config = config;
  }
  getSettings(device) {
    return co(function*() {
      return yield SettingsModel.findById('_id', device._id);
    });
  }
  addSettings(stng) {
    return co(function*() {
      var settings = yield SettingsModel.find('_id', stng._id);
      if (settings.length == 0) {
        let newSettings = new SettingsModel({
          imageInterval: stng.imageInterval,
          gpsInterval: stng.gpsInterval,
          scheduledTime: JSON.parse(JSON.stringify(stng.scheduledTime)),
          socketIp: stng.socketIp,
          vehicleNo: stng.vehicleNo
        });
        return yield newSettings.save();
      } else {
        console.log("DEVICE SETTING ALREADY ADDED");
        return;
      }

    });
  }
  updateSetttings(stng) {
    return co(function*() {
      let setting = yield SettingsModel.find('_id', stng._id);
      if (setting.length > 0) {
        setting[0].set({
          _id: stng._id,
          imageInterval: stng.imageInterval,
          gpsInterval: stng.gpsInterval,
          scheduledTime: JSON.parse(JSON.stringify(stng.scheduledTime)),
          socketIp: stng.socketIp,
          vehicleNo: stng.vehicleNo
        });
        return yield settings[0].save()
      } else {
        return;
      }
    });
  }
}
module.exports = SettingData;
