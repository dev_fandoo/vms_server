const Settings = require('./libs/SettingsData');
const config = require('../config');
class DeviceSettings {
  constructor() {
    this.Settings = new Settings(config);
  }
  getDeviceSettings(device) {
    return this.Settings.getSettings(device);
  }
  saveDeviceSetting(device) {
    return this.Settings.addSettings(device);
  }
  updateDeviceSetting(device) {
    return this.Settings.updateSetttings(device);
  }
}
module.exports = DeviceSettings;
