const fs = require('fs');
const device = require('./Models/device');
const path = require('path');
exports.startSocket = function (io) {

    io.use(function (socket, next) {
        try {
            // var handshakeData = socket.request;
            // console.log(socket.handshake.auth_token);

            checkAuthToken(socket.handshake.query.auth_token, function (err, success) {
                if (!err && success) {
                    console.log("Authenticated socket ", socket.id);
                    socket.auth = true;
                    next();
                } else {
                    socket.auth = false;
                    console.log("Socket not Authenticated ", socket.id);
                    next(new Error('Authentication error'));
                }
            });
        } catch (err) {
            console.error(err.stack);
            next(new Error('Internal server error'));
        }
    });
    io.on('connection', function (socket) {
        var socketId = socket.id;
        var clientIp = socket.request.connection.remoteAddress;
        var deviceLocation = {};
        socket.on('chtmessage', function (message) {

            message = JSON.parse(message);
            // console.log(message.latitude, io.engine.clientsCount);
            // if (message.latitude == 0) {
            let dir = '/uploads/';
            let fileName = "arghhhh33-" + Date.now() + ".webp"
            console.log(fileName);
            // if (!path.existsSync(dir)) {
            //     fs.mkdirSync(dir, 0744);
            // }

            fs.writeFile(fileName, new Buffer(message.imageData, 'base64'), function (err) {
                if (!err) {
                    //'http://localhost:3000/' +
                    device.createDevice(fileName, function (err) {
                        if (err) {
                            console.error(err);
                        } else {
                            console.log('Saved');
                        }
                    })
                }
            });
            //}
        });
        socket.on('disconnect', function (usr) {
            console.log(socket.id + ' user disconnected');
        });
        socket.on('error', function (error) {
            socket.disconnect();
        });
    });
}
var checkAuthToken = function (token, callback) {

    if (token == 'YelloJi') {
        callback(false, true);
    } else {
        callback(false, false);
    }
}
