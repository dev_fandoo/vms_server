const ProcessData = require('./libs/processData');
const run = require('../util/run');
class Location {
  processSaveData(userId, locationData) {
    let data = new ProcessData(userId, locationData);
    return data.save().catch(function(err) {
      console.error(err.stack);
    });
  }
}


module.exports = Location;
