const CommandsModel = require('../models/Commands');
var co = require('co');
const push_ = require('../../socket');
const cmd_push = require('../../emmiter');

class Command {
  constructor(id) {
    this.cmd = {}
    this.cmd.userid = id.userid
    this.cmd.type = id.type;
    this.cmd.payload = id.payload;
    this._saveToDb();
  }
  static push(user) {
    const socket = require('../../app');
    socket.emitCommand('58380e33bdc0100b14b19d6b', 'tdes');
  }

  _saveToDb() {
    co(function*() {
      let newCommand = new CommandsModel({
        type: this.type,
        payload: this.payload
      });
      yield newCommand.save();
    });
  }
  toString() {
    JSON.stringify({
      type: this.type,
      payload: this.payload
    });
  }
}
module.exports = Command;
