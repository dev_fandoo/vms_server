const DeviceModel = require('../model/device');
class ProcessDeviceData {
  constructor() {

  }
  saveDevice(device) {
    return co(function*() {
      let devices = yield DeviceModel.find({
        "imei": device.imei,
        "deviceId": device.deviceId
      });
      if (devices.length == 0) {
        let newDevice = new DeviceModel({
          'imei': device.imei,
          'deviceId': device.deviceId,
          "verified": false
        });
        return yield newDevice.save();
      } else {
        return;
      }
    });
  }
  getDevice(id) {
    return co(function*() {
      return yield DeviceModel.findById('_id', id);
    });
  }
  verifyDevice(device) {
    return co(function*() {
      let devices = yield DeviceModel.find({
        "imei": device.imei,
        "deviceId": device.deviceId
      });
      if (devices.length > 0) {
        devices[0].set('verified', true);
        return yield devices[0].save();
      } else {
        return;
      }
    });
  }
}
module.exports = ProcessDeviceData;
