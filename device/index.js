const ProcessData = require('./libs/DeviceData');
class DeviceData {
  constructor() {
    this.processData = new ProcessData();
  }
  saveDeviceInfo(device) {
    return this.processData.saveDevice(device);
  }
  getDeviceById(id) {
    return this.processData.getDevice(id)
  }
  verifyDevice(device) {
    return this.processData.verifyDevice(device);
  }
}
module.exports = DeviceData;
